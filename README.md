# A software modelling project

Language: Polish

A software model documentation (in polish) created using AsciiDoc and plantUML (diagrams served from a local instance of Kroki, installed using Docker, for first experience with containers and increased privacy), as part of _Software Engineering_ subject, during my CS studies.

The PDF contains an intended output, produced with a custom `wkhtmltopdf`, as there were some problems with automatic export from within VS Codium.